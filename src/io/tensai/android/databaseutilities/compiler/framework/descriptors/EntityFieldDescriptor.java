/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.framework.descriptors;

import io.tensai.java.annotationprocessorutilities.template.utils.StringUtils;
import io.tensai.java.annotationprocessorutilities.util.TypeUtils;
import io.tensai.java.annotationprocessorutilities.wrappers.ClassFieldElementWrapper;
import io.tensai.android.databaseutilities.interfaces.annotations.repository.*;

import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import java.util.Date;

/**
 * Created by aston
 */
public class EntityFieldDescriptor {
    public enum FieldType {String, Date, Short, Char, Int, Long, Float, Double, Boolean}

    private final ClassFieldElementWrapper mSourceFieldElement;
    private final int mFieldPosition;

    public EntityFieldDescriptor(final ClassFieldElementWrapper sourceFieldElement, final int fieldPosition) {
        mSourceFieldElement = sourceFieldElement;
        mFieldPosition = fieldPosition;
    }

    public String getName() {
        final String rawName = mSourceFieldElement.getSimpleName();
        if (mSourceFieldElement.hasAnnotation(EntityField.class)) {
            return mSourceFieldElement.getAnnotation(EntityField.class).name();
        }

        if (rawName.length() > 1 && Character.isLowerCase(rawName.charAt(0)) && Character.isUpperCase(rawName.charAt(1))) {
            return StringUtils.getInstance().camel_to_snake_case(rawName.substring(1)).toLowerCase();
        }

        return StringUtils.getInstance().camel_to_snake_case(rawName).toLowerCase();
    }

    public String getExplicitName() {
        return mSourceFieldElement.getAnnotation(EntityField.class) == null ?
                null : mSourceFieldElement.getAnnotation(EntityField.class).name();
    }

    public String getRawName() {
        return mSourceFieldElement.getSimpleName();
    }

    public String getExpectedParameterName() {
        final String rawName = mSourceFieldElement.getSimpleName();
        if (rawName.length() > 1 && Character.isLowerCase(rawName.charAt(0)) && Character.isUpperCase(rawName.charAt(1))) {
            return Character.toLowerCase(rawName.charAt(1)) + rawName.substring(2);
        }

        return rawName;
    }

    public boolean isLikePrimaryKey() {
        boolean likePrimaryKey = false;

        switch (getName()) {
            case "id":
            case "pk":
            case "_id":
            case "_pk":
                likePrimaryKey = true;
                break;
        }

        return likePrimaryKey && mSourceFieldElement.getVariableType() == TypeKind.LONG;
    }

    public boolean isExplicitPrimaryKey() {
        return mSourceFieldElement.hasAnnotation(EntityPrimaryKey.class);
    }

    public IndexedEntityField[] getIndexDeclarations() {
        if (mSourceFieldElement.hasAnnotation(EntityFieldIndexes.class)) {
            if (mSourceFieldElement.hasAnnotation(IndexedEntityField.class)) {
                final EntityFieldIndexes compoundDeclaration = mSourceFieldElement.getAnnotation(EntityFieldIndexes.class);
                final IndexedEntityField[] declarations = new IndexedEntityField[compoundDeclaration.value().length + 1];
                System.arraycopy(compoundDeclaration.value(), 0, declarations, 0, compoundDeclaration.value().length);
                declarations[compoundDeclaration.value().length] = mSourceFieldElement.getAnnotation(IndexedEntityField.class);
                return declarations;
            }

            return mSourceFieldElement.getAnnotation(EntityFieldIndexes.class).value();
        }

        if (mSourceFieldElement.hasAnnotation(IndexedEntityField.class)) {
            return new IndexedEntityField[]{mSourceFieldElement.getAnnotation(IndexedEntityField.class)};
        }

        return new IndexedEntityField[0];
    }

    public FieldType getFieldType() {
        return EntityFieldDescriptor.getFieldTypeFromTypeMirror(mSourceFieldElement.getSourceElement().asType());
    }

    public String getCursorAccessor() {
        switch (getFieldType()) {
            case Short:
                return String.format("%%s.getShort(%s)", mFieldPosition);
            case Char:
                return String.format("%%s.getString(%s).charAt(0)", mFieldPosition);
            case Long:
                return String.format("%%s.getLong(%s)", mFieldPosition);
            case Int:
                return String.format("%%s.getInt(%s)", mFieldPosition);
            case Float:
                return String.format("%%s.getFloat(%s)", mFieldPosition);
            case Double:
                return String.format("%%s.getDouble(%s)", mFieldPosition);
            case Boolean:
                return String.format("%%s.getBoolean(%s)", mFieldPosition);
            case String:
                return String.format("%%s.getString(%s)", mFieldPosition);
            case Date:
                return String.format("new Date(%%s.getLong(%s))", mFieldPosition);
        }
        return null;
    }

    public String getGetterName() {
        final String camelCaseFieldName = StringUtils.getInstance().snake_to_camel_case(getName());
        return "get" + Character.toUpperCase(camelCaseFieldName.charAt(0)) + (camelCaseFieldName.length() > 1 ? camelCaseFieldName.substring(1) : "");
    }

    public String getGetterJavaType() {
        return EntityFieldDescriptor.getJavaTypeFromFieldType(getFieldType());
    }

    public String getConventValuesGetterName() {
        switch (getFieldType()) {
            case Short:
                return "getAsShort";
            case Char:
                return "getAsString";
            case Long:
                return "getAsLong";
            case Int:
                return "getAsInteger";
            case Float:
                return "getAsFloat";
            case Double:
                return "getAsDouble";
            case Boolean:
                return "getAsBoolean";
            case String:
                return "getAsString";
            case Date:
                return "getAsLong";
        }
        return null;
    }

    public String getContentValuesGetterWrapperPrefix() {
        switch (getFieldType()) {
            case Short:
            case Char:
            case Long:
            case Int:
            case Float:
            case Double:
            case Boolean:
            case String:
                return "";
            case Date:
                return "new Date(";
        }
        return null;
    }

    public String getContentValuesGetterWrapperSuffix() {
        switch (getFieldType()) {
            case Char:
                return ".charAt(0)";
            case Short:
            case Long:
            case Int:
            case Float:
            case Double:
            case Boolean:
            case String:
                return "";
            case Date:
                return ")";
        }
        return null;
    }

    public String getSetterName() {
        final String camelCaseFieldName = StringUtils.getInstance().snake_to_camel_case(getName());
        return "set" + Character.toUpperCase(camelCaseFieldName.charAt(0)) + (camelCaseFieldName.length() > 1 ? camelCaseFieldName.substring(1) : "");
    }

    public String getSetterJavaType() {
        return EntityFieldDescriptor.getJavaTypeFromFieldType(getFieldType());
    }

    public String getSetterValue() {
        switch (getFieldType()) {
            case Short:
            case Char:
            case Long:
            case Int:
            case Float:
            case Double:
            case Boolean:
            case String:
                return "%s";
            case Date:
                return "%s.getTime()";
        }
        return null;
    }

    public String getSQLType() {
        switch (getFieldType()) {
            case Short:
            case Long:
            case Int:
            case Boolean:
            case Date:
                return "INTEGER";

            case Float:
            case Double:
                return "REAL";

            case Char:
            case String:
                return "TEXT";
        }

        return null;
    }

    public boolean isAutoIncrement() {
        return mSourceFieldElement.hasAnnotation(AutoIncrementedEntityField.class);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final EntityFieldDescriptor that = (EntityFieldDescriptor) o;

        final FieldType thisType = getFieldType(), thatType = that.getFieldType();
        final String thisName = getName(), thatName = that.getName();

        return (thisType != null ? thisType.equals(thatType) : thatType == null)
                && (thisName != null ? thisName.equals(thatName) : thatName == null);

    }

    @Override
    public int hashCode() {
        return mSourceFieldElement != null ? mSourceFieldElement.hashCode() : 0;
    }

    public static String getJavaTypeFromFieldType(final FieldType fieldType) {
        switch (fieldType) {
            case Short:
                return "short";
            case Char:
                return "char";
            case Long:
                return "long";
            case Int:
                return "int";
            case Float:
                return "float";
            case Double:
                return "double";
            case Boolean:
                return "boolean";
            case String:
                return "String";
            case Date:
                return "Date";
        }

        return null;
    }

    public static FieldType getFieldTypeFromTypeMirror(final TypeMirror typeMirror) {
        switch (typeMirror.getKind()) {
            case SHORT:
                return FieldType.Short;
            case CHAR:
                return FieldType.Char;
            case LONG:
                return FieldType.Long;
            case INT:
                return FieldType.Int;
            case FLOAT:
                return FieldType.Float;
            case DOUBLE:
                return FieldType.Double;
            case BOOLEAN:
                return FieldType.Boolean;
            case DECLARED:
                return TypeUtils.isDeclaredType(typeMirror, String.class) ? FieldType.String
                        : (TypeUtils.isDeclaredType(typeMirror, Date.class) ? FieldType.Date : null);
        }

        return null;
    }

    @Override
    public String toString() {
        return "Field{" +
                mFieldPosition +
                ", " + getFieldType() + ", " + getName() +
                '}';
    }
}
