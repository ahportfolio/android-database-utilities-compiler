/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.framework.descriptors;

import io.tensai.java.annotationprocessorutilities.wrappers.ClassElementWrapper;
import io.tensai.java.annotationprocessorutilities.wrappers.ClassFieldElementWrapper;
import io.tensai.android.databaseutilities.interfaces.annotations.repository.*;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import java.util.*;

/**
 * Created by aston
 */
public class EntityDescriptor {
    private final ClassElementWrapper mSourceClassElement;
    private final SQLiteEntity mSQLiteEntityAnnotation;
    private final EntityFieldDescriptor mPrimaryKey;
    private final EntityConstructorDescriptor mConstructor;
    private final FieldIndex[] mIndices;

    private EntityFieldDescriptor[] mFieldElementsCache;

    public EntityDescriptor(final ClassElementWrapper sourceClassElement) {
        mSourceClassElement = sourceClassElement;
        mSQLiteEntityAnnotation = sourceClassElement.getAnnotation(SQLiteEntity.class);

        mPrimaryKey = _findEntityPrimaryKey();
        mConstructor = _findEntityConstructor();
        mIndices = _buildEntityIndices();
    }

    private FieldIndex[] _buildEntityIndices() {
        final EntityFieldDescriptor[] fields = getFields();
        final Map<String, FieldIndex> indexMap = new HashMap<>(fields.length);

        for (final EntityFieldDescriptor field : fields) {
            for (final IndexedEntityField indexedEntityField : field.getIndexDeclarations()) {
                final String indexName = indexedEntityField.name().length() == 0 ? "idx_" + field.getName() : indexedEntityField.name();
                final FieldIndex fieldIndex;
                if (indexMap.containsKey(indexName)) {
                    fieldIndex = indexMap.get(indexName);
                } else {
                    fieldIndex = new FieldIndex(indexName, fields.length);
                    indexMap.put(indexName, fieldIndex);
                }

                fieldIndex.addField(field);
                if (indexedEntityField.unique() != IndexedEntityField.Unique.UNDEFINED) {
                    fieldIndex.setIsUnique(indexedEntityField.unique() == IndexedEntityField.Unique.UNIQUE);
                }
                if (indexedEntityField.criteria().length() > 0) {
                    fieldIndex.setCriteria(indexedEntityField.criteria());
                }
            }
        }


        return indexMap.values().toArray(new FieldIndex[indexMap.size()]);
    }

    private EntityConstructorDescriptor _findEntityConstructor() {
        EntityConstructorDescriptor entityConstructor = null;

        final EntityFieldDescriptor[] entityFields = getFields();


        final EntityFieldDescriptor[] entityConstructorParameters = new EntityFieldDescriptor[entityFields.length];
        constructorBingingLoop:
        for (final ExecutableElement constructor : ElementFilter.constructorsIn(mSourceClassElement.getSourceElement().getEnclosedElements())) {
            if (constructor.getAnnotation(EntityConstructor.class) == null && entityConstructor != null) {
                continue;
            }

            final List<? extends VariableElement> constructorParameters = constructor.getParameters();

            if (constructorParameters.size() != entityFields.length) {
                continue;
            }

            parameterBindingLoop:
            for (int i = 0; i < constructorParameters.size(); i++) {
                final VariableElement parameter = constructorParameters.get(i);

                if (parameter.getAnnotation(BoundedToEntityField.class) != null) {
                    final String boundedFieldName = parameter.getAnnotation(BoundedToEntityField.class).field();
                    for (final EntityFieldDescriptor entityField : entityFields) {
                        if (boundedFieldName.equals(entityField.getExplicitName())
                                || boundedFieldName.equals(entityField.getRawName())
                                || boundedFieldName.equals(entityField.getExpectedParameterName())
                                || boundedFieldName.equals(entityField.getName())) {
                            entityConstructorParameters[i] = entityField;
                            continue parameterBindingLoop;
                        }
                    }
                }

                final EntityFieldDescriptor.FieldType parameterType = EntityFieldDescriptor.getFieldTypeFromTypeMirror(parameter.asType());
                final String parameterName = parameter.getSimpleName().toString();

                for (final EntityFieldDescriptor entityField : entityFields) {
//                    DatabaseAnnotationProcessor.mToolkit.getLogger().debug("=======> Parameter %s %s test against %s %s", parameterType, parameterName, entityField.getFieldType(), entityField.getExpectedParameterName());
                    if (parameterType == entityField.getFieldType()
                            && (parameterName.equals(entityField.getExplicitName())
                            || parameterName.equals(entityField.getRawName())
                            || parameterName.equals(entityField.getExpectedParameterName())
                            || parameterName.equals(entityField.getName()))) {
                        entityConstructorParameters[i] = entityField;
                        continue parameterBindingLoop;
                    }
                }


                continue constructorBingingLoop;
            }

            entityConstructor = new EntityConstructorDescriptor(entityConstructorParameters);

            if (constructor.getAnnotation(EntityConstructor.class) != null) {
                break;
            }
        }

        return entityConstructor;
    }

    private EntityFieldDescriptor _findEntityPrimaryKey() {
        EntityFieldDescriptor primaryKey = null;

        for (final EntityFieldDescriptor fieldElement : getFields()) {
            if (fieldElement.isExplicitPrimaryKey()) {
                primaryKey = fieldElement;
                break;
            } else if (primaryKey == null && fieldElement.isLikePrimaryKey()) {
                primaryKey = fieldElement;
            }
        }

        return primaryKey;
    }

    public String getName() {
        return mSourceClassElement.getSimpleName();
    }

    public String getTableName() {
        final String tableName = mSQLiteEntityAnnotation.name().trim();
        return tableName.length() == 0 ? getName().toLowerCase() : tableName;
    }

    public String getDatabaseName() {
        final String databaseName = mSQLiteEntityAnnotation.database().trim();
        return databaseName.length() == 0 ? null : databaseName;
    }

    public FieldIndex[] getIndices() {
        return mIndices;
    }

    public EntityConstructorDescriptor getConstructor() {
        return mConstructor;
    }

    public EntityFieldDescriptor getPrimaryKey() {
        return mPrimaryKey;
    }

    public String getEntityPackageName() {
        final int dotLastIndex = mSourceClassElement.getSourceElement().getQualifiedName().toString().lastIndexOf('.');
        if (dotLastIndex < 0) {
            return null;
        }

        final String packageName = mSourceClassElement.getSourceElement().getQualifiedName().toString().substring(0, dotLastIndex);
        return packageName.length() == 0 ? null : packageName;
    }

    public EntityFieldDescriptor[] getFields() {
        if (mFieldElementsCache == null) {
            final ArrayList<EntityFieldDescriptor> fieldElementWrappers = new ArrayList<>();

            final ClassFieldElementWrapper[] fields = mSourceClassElement.getFields();
            for (int i = 0, fieldsLength = fields.length; i < fieldsLength; i++) {
                final ClassFieldElementWrapper field = fields[i];
                if (!field.hasAnnotation(ExcludedEntityField.class)) {
                    fieldElementWrappers.add(new EntityFieldDescriptor(field, i));
                }
            }

            mFieldElementsCache = fieldElementWrappers.toArray(new EntityFieldDescriptor[fieldElementWrappers.size()]);
        }

        return mFieldElementsCache;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final EntityDescriptor that = (EntityDescriptor) o;

        return mSourceClassElement != null ? mSourceClassElement.equals(that.mSourceClassElement) : that.mSourceClassElement == null;

    }

    @Override
    public int hashCode() {
        return mSourceClassElement != null ? mSourceClassElement.hashCode() : 0;
    }

    public Object getQualifiedName() {
        return mSourceClassElement.getSourceElement().getQualifiedName();
    }

    @Override
    public String toString() {
        return mSourceClassElement.getSourceElement().getQualifiedName() + "{\n" +
                Arrays.toString(getFields()) +
                "\n" + getConstructor() +
                "\nPrimaryKey=" + getPrimaryKey() +
                "\nIndicies=" + Arrays.toString(getIndices()) +
                '}';
    }

    public static class FieldIndex {
        private final String mName;
        private boolean mIsUnique;
        private String mCriteria;
        private Set<EntityFieldDescriptor> mFields;

        public FieldIndex(final String name, final int fieldCapacity) {
            mName = name;
            mIsUnique = false;
            mCriteria = "";
            mFields = new HashSet<>(fieldCapacity);
        }

        public String getName() {
            return mName;
        }

        public boolean isUnique() {
            return mIsUnique;
        }

        public void setIsUnique(final boolean unique) {
            mIsUnique = unique;
        }

        public String getCriteria() {
            return mCriteria;
        }

        public void setCriteria(final String value) {
            mCriteria = value;
        }

        public void addField(final EntityFieldDescriptor field) {
            mFields.add(field);
        }

        public Set<EntityFieldDescriptor> getFields() {
            return Collections.unmodifiableSet(mFields);
        }

        @Override
        public String toString() {
            return "FieldIndex{" +
                    "Name='" + mName + '\'' +
                    ", IsUnique=" + mIsUnique +
                    ", Criteria='" + mCriteria + '\'' +
                    ", Fields=" + mFields +
                    '}';
        }
    }
}
