/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.framework.descriptors;

import io.tensai.java.annotationprocessorutilities.wrappers.ClassElementWrapper;
import io.tensai.android.databaseutilities.interfaces.annotations.repository.SQLiteDatabase;

import java.util.List;

/**
 * Created by aston
 */
public class SQLiteDatabaseDescriptor {
    private final ClassElementWrapper mSourceClassElement;
    private final SQLiteDatabase mDatabaseAnnotation;
    private final List<EntityDescriptor> mEntities;

    public SQLiteDatabaseDescriptor(final ClassElementWrapper sourceClassElement, final List<EntityDescriptor> entities) {
        mSourceClassElement = sourceClassElement;
        mEntities = entities;
        mDatabaseAnnotation = sourceClassElement.getAnnotation(SQLiteDatabase.class);
    }

    public String getFileName() {
        return SQLiteDatabaseDescriptor.getDatabaseName(mSourceClassElement);
    }

    public String getOpenHelperPackageName() {
        return mSourceClassElement.getPackageName();
    }

    public String getOpenHelperSimpleName() {
        return mSourceClassElement.getSimpleName() + "SQLiteOpenHelper";
    }

    public String getOpenHelperQualifiedName() {
        return mSourceClassElement.getQualifiedName() + "SQLiteOpenHelper";
    }

    public int getVersion() {
        return mDatabaseAnnotation.version();
    }

    public List<EntityDescriptor> getEntities() {
        return mEntities;
    }

    public static String getDatabaseName(final ClassElementWrapper clazz) {
        final SQLiteDatabase databaseAnnotation = clazz.getAnnotation(SQLiteDatabase.class);
        if (databaseAnnotation == null) {
            return clazz.getSimpleName() + ".db";
        }
        final String databaseName = databaseAnnotation.name().trim();
        return databaseName.length() == 0 ? clazz.getSimpleName() + ".db" : databaseName;
    }

    @Override
    public String toString() {
        return mSourceClassElement.getSourceElement().getQualifiedName() + "{" +
                "\nName=" + getFileName() +
                "\nEntities:\n" + strJoin(mEntities, "\n\n") +
                '}';
    }

    public static String strJoin(List<?> list, String sSep) {
        StringBuilder sbStr = new StringBuilder();
        for (int i = 0, il = list.size(); i < il; i++) {
            if (i > 0)
                sbStr.append(sSep);
            sbStr.append(list.get(i).toString());
        }
        return sbStr.toString();
    }
}
