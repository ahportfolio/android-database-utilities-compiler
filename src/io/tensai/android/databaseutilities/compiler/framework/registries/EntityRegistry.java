/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.framework.registries;

import io.tensai.android.databaseutilities.compiler.framework.descriptors.EntityDescriptor;

import java.util.*;

/**
 * Created by aston
 */
public class EntityRegistry {


    private Map<String, List<EntityDescriptor>> mRegistry = new HashMap<>();

    public List<EntityDescriptor> getEntities(final String databaseName) {
        if (!mRegistry.containsKey(databaseName)) {
            return Collections.emptyList();
        }

        return Collections.unmodifiableList(mRegistry.get(databaseName));
    }

    public void addEntity(final EntityDescriptor entity) {
        if (mRegistry.containsKey(entity.getDatabaseName())) {
            mRegistry.get(entity.getDatabaseName()).add(entity);
        } else {
            final List<EntityDescriptor> entityList = new ArrayList<>();
            entityList.add(entity);
            mRegistry.put(entity.getDatabaseName(), entityList);
        }
    }

    public void clear() {
        mRegistry.clear();
    }

    public static final class Singleton {
        private static EntityRegistry INSTANCE;

        public static EntityRegistry getInstance() {
            if (INSTANCE == null) {
                INSTANCE = new EntityRegistry();
            }

            return INSTANCE;
        }
    }
}
