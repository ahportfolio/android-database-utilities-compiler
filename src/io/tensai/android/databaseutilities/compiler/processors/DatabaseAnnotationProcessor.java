/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.processors;


import io.tensai.java.annotationprocessorutilities.filewriters.ClassFileTemplateWriter;
import io.tensai.java.annotationprocessorutilities.processors.AbstractComposableAnnotationProcessor;
import io.tensai.android.databaseutilities.compiler.processors.units.GenerateDatabaseInterfacesProcessorUnit;
import io.tensai.android.databaseutilities.compiler.processors.units.GenerateEntityManagerProcessorUnit;
import io.tensai.android.databaseutilities.compiler.processors.units.GenerateEntityRepositoriesProcessorUnit;
import io.tensai.android.databaseutilities.compiler.processors.units.GenerateSQLiteOpenHelpersProcessorUnit;
import io.tensai.android.databaseutilities.compiler.templates.TemplateRoot;

public class DatabaseAnnotationProcessor extends AbstractComposableAnnotationProcessor {
    public DatabaseAnnotationProcessor() {
        super(
                new GenerateDatabaseInterfacesProcessorUnit(),
                new GenerateEntityRepositoriesProcessorUnit(),
                new GenerateSQLiteOpenHelpersProcessorUnit(),
                new GenerateEntityManagerProcessorUnit()
        );
        ClassFileTemplateWriter.setTemplateResourceLoaderClass(TemplateRoot.class);
    }
}
