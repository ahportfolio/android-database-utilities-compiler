/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.processors.units;

import io.tensai.java.annotationprocessorutilities.exceptions.ProcessorException;
import io.tensai.java.annotationprocessorutilities.filewriters.ClassFileTemplateWriter;
import io.tensai.java.annotationprocessorutilities.interfaces.wrappers.IElementWrapper;
import io.tensai.java.annotationprocessorutilities.processors.units.SimpleAnnotationProcessorUnit;
import io.tensai.java.annotationprocessorutilities.template.utils.StringUtils;
import io.tensai.java.annotationprocessorutilities.util.Logger;
import io.tensai.java.annotationprocessorutilities.util.ProcessorToolkit;
import io.tensai.java.annotationprocessorutilities.wrappers.ClassElementWrapper;
import io.tensai.android.databaseutilities.interfaces.annotations.repository.SQLiteDatabase;
import io.tensai.android.databaseutilities.compiler.framework.descriptors.SQLiteDatabaseDescriptor;
import io.tensai.android.databaseutilities.compiler.framework.registries.DatabaseRegistry;
import io.tensai.android.databaseutilities.compiler.framework.registries.EntityRegistry;

import javax.annotation.processing.RoundEnvironment;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by aston
 */
public class GenerateSQLiteOpenHelpersProcessorUnit extends SimpleAnnotationProcessorUnit {
    public GenerateSQLiteOpenHelpersProcessorUnit() {
        super(new AnnotationQuery(SQLiteDatabase.class, IElementWrapper.WrapperType.Class));
    }

    @Override
    public void processEnvironment(final RoundEnvironment env, final ProcessorToolkit toolkit) {
        DatabaseRegistry.Singleton.getInstance().clear();
        super.processEnvironment(env, toolkit);
    }

    @Override
    protected void onClassElementFound(final ClassElementWrapper element, final ProcessorToolkit toolkit) {
        final Logger logger = toolkit.getLogger();
        logger.info("===========> Found database: %s", element.getSourceElement().getQualifiedName().toString());

        try {
            final SQLiteDatabaseDescriptor database = new SQLiteDatabaseDescriptor(
                    element, EntityRegistry.Singleton.getInstance().getEntities(SQLiteDatabaseDescriptor.getDatabaseName(element)));

            final Map<String, ?> templateEnvironment;

            logger.info("Detection: " + String.format("Database analysis complete: %s", database));

            templateEnvironment = new HashMap<String, Object>() {
                {
                    put("str_util", StringUtils.getInstance());
                    put("database", database);
                }
            };

            logger.debug("Generating: " + String.format("%s.", database.getOpenHelperQualifiedName()));
            new ClassFileTemplateWriter(
                    String.format("%s", database.getOpenHelperQualifiedName()),
                    "database/TplSQLiteDatabaseOpenHelper.java.tpl",
                    templateEnvironment
            ).writeFile(toolkit.getFiler(), element.getSourceElement());

            DatabaseRegistry.Singleton.getInstance().addDatabase(database);
        } catch (final Exception e) {
            throw new ProcessorException(e);
        }

        super.onClassElementFound(element, toolkit);
    }
}
