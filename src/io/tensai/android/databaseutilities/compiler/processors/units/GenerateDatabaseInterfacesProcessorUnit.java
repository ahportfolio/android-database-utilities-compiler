/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.processors.units;

import io.tensai.java.annotationprocessorutilities.interfaces.wrappers.IElementWrapper;
import io.tensai.java.annotationprocessorutilities.processors.units.SimpleAnnotationProcessorUnit;
import io.tensai.java.annotationprocessorutilities.util.ProcessorToolkit;
import io.tensai.android.databaseutilities.interfaces.annotations.repository.SQLiteEntity;

import javax.annotation.processing.RoundEnvironment;

/**
 * Created by aston
 */
public class GenerateDatabaseInterfacesProcessorUnit extends SimpleAnnotationProcessorUnit {
    public GenerateDatabaseInterfacesProcessorUnit() {
        super(new AnnotationQuery(SQLiteEntity.class, IElementWrapper.WrapperType.Class));
    }

    @Override
    public void processEnvironment(final RoundEnvironment env, final ProcessorToolkit toolkit) {
        if (toolkit.getProcessingRound() > 1) {
            return;
        }

//        try {
//            toolkit.getLogger().debug("Generating: IEntityRepository: base interface");
//            new ClassFileCopiedWriter(
//                    "io.tensai.android.databaseutilities.interfaces.repository.IEntityRepository",
//                    TemplateRoot.class.getResource("repository/IEntityRepository.java.tpl").getPath()
//            ).writeFile(toolkit.getFiler());
//
//            toolkit.getLogger().debug("Generating: IContentValuesBuilder: base interface");
//            new ClassFileCopiedWriter(
//                    "io.tensai.android.databaseutilities.interfaces.util.IContentValuesBuilder",
//                    TemplateRoot.class.getResource("util/IContentValuesBuilder.java.tpl").getPath()
//            ).writeFile(toolkit.getFiler());
//
//            toolkit.getLogger().debug("Generating: AbstractEntityRepository: base class");
//            new ClassFileCopiedWriter(
//                    "io.tensai.android.databaseutilities.dbal.repository.AbstractEntityRepository",
//                    TemplateRoot.class.getResource("repository/AbstractEntityRepository.java.tpl").getPath()
//            ).writeFile(toolkit.getFiler());
//        } catch (final IOException e) {
//            throw new ProcessorException(e);
//        }

        super.processEnvironment(env, toolkit);
    }

}
