/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.processors.units;

import io.tensai.java.annotationprocessorutilities.exceptions.ProcessorException;
import io.tensai.java.annotationprocessorutilities.filewriters.ClassFileTemplateWriter;
import io.tensai.java.annotationprocessorutilities.interfaces.wrappers.IElementWrapper;
import io.tensai.java.annotationprocessorutilities.processors.units.SimpleAnnotationProcessorUnit;
import io.tensai.java.annotationprocessorutilities.template.utils.StringUtils;
import io.tensai.java.annotationprocessorutilities.util.Logger;
import io.tensai.java.annotationprocessorutilities.util.ProcessorToolkit;
import io.tensai.android.databaseutilities.interfaces.annotations.repository.SQLiteEntity;
import io.tensai.android.databaseutilities.interfaces.annotations.repository.SQLiteDatabase;
import io.tensai.android.databaseutilities.compiler.framework.registries.DatabaseRegistry;

import javax.annotation.processing.RoundEnvironment;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by aston
 */
public class GenerateEntityManagerProcessorUnit extends SimpleAnnotationProcessorUnit {
    public GenerateEntityManagerProcessorUnit() {
        super(new AnnotationQuery(SQLiteDatabase.class, IElementWrapper.WrapperType.Class),
                new AnnotationQuery(SQLiteEntity.class, IElementWrapper.WrapperType.Class));
    }

    @Override
    public void processEnvironment(final RoundEnvironment env, final ProcessorToolkit toolkit) {
        if (toolkit.getProcessingRound() != 1) {
            super.processEnvironment(env, toolkit);
            return;
        }

        final Logger logger = toolkit.getLogger();

        try {
            final Map<String, ?> templateEnvironment;

            templateEnvironment = new HashMap<String, Object>() {
                {
                    put("str_util", StringUtils.getInstance());
                    put("databases", DatabaseRegistry.Singleton.getInstance().getDatabases());
                }
            };

            logger.debug("Generating: io.tensai.android.databaseutilities.dbal.repository.DefaultEntityRepositoryFactory");
            new ClassFileTemplateWriter(
                    "io.tensai.android.databaseutilities.dbal.repository.DefaultEntityRepositoryFactory",
                    "repository/TplDefaultEntityRepositoryFactory.java.tpl",
                    templateEnvironment
            ).writeFile(toolkit.getFiler());

        } catch (final Exception e) {
            throw new ProcessorException(e);
        }

        super.processEnvironment(env, toolkit);
    }
}
