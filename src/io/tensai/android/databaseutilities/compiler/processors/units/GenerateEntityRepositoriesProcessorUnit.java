/*
 *     Copyright (C) 2016  Aston Hamilton
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.tensai.android.databaseutilities.compiler.processors.units;

import io.tensai.java.annotationprocessorutilities.exceptions.ProcessorException;
import io.tensai.java.annotationprocessorutilities.filewriters.ClassFileTemplateWriter;
import io.tensai.java.annotationprocessorutilities.interfaces.wrappers.IElementWrapper;
import io.tensai.java.annotationprocessorutilities.processors.units.SimpleAnnotationProcessorUnit;
import io.tensai.java.annotationprocessorutilities.template.utils.StringUtils;
import io.tensai.java.annotationprocessorutilities.util.Logger;
import io.tensai.java.annotationprocessorutilities.util.ProcessorToolkit;
import io.tensai.java.annotationprocessorutilities.wrappers.ClassElementWrapper;
import io.tensai.android.databaseutilities.interfaces.annotations.repository.SQLiteEntity;
import io.tensai.android.databaseutilities.compiler.framework.descriptors.EntityDescriptor;
import io.tensai.android.databaseutilities.compiler.framework.registries.EntityRegistry;

import javax.annotation.processing.RoundEnvironment;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aston
 */
public class GenerateEntityRepositoriesProcessorUnit extends SimpleAnnotationProcessorUnit {
    public GenerateEntityRepositoriesProcessorUnit() {
        super(new AnnotationQuery(SQLiteEntity.class, IElementWrapper.WrapperType.Class));
    }

    @Override
    public void processEnvironment(final RoundEnvironment env, final ProcessorToolkit toolkit) {
        EntityRegistry.Singleton.getInstance().clear();
        super.processEnvironment(env, toolkit);
    }

    @Override
    protected void onClassElementFound(final ClassElementWrapper element, final ProcessorToolkit toolkit) {
        final Logger logger = toolkit.getLogger();
        logger.info("===========> Found entity: %s", element.getSourceElement().getQualifiedName().toString());

        try {
            final EntityDescriptor entity = new EntityDescriptor(element);

            final Map<String, ?> templateEnvironment;

            if (entity.getPrimaryKey() == null) {
                logger.error(String.format("Detection: No primary key was found in the '%s' entity", entity.getQualifiedName()));
                return;
            }

            if (entity.getConstructor() == null) {
                logger.error(String.format("Detection: No appropriate constructor was found in the '%s' entity", entity.getQualifiedName()));
                return;
            }

            logger.info("Detection: " + String.format("Entity analysis complete: %s", entity));

            templateEnvironment = new HashMap<String, Object>() {
                {
                    put("str_util", StringUtils.getInstance());
                    put("entity", entity);
                }
            };

            logger.debug("Generating: " + String.format("%sContract", entity.getQualifiedName()));
            new ClassFileTemplateWriter(
                    String.format("%sContract", entity.getQualifiedName()),
                    "contract/TplEntityContract.java.tpl",
                    templateEnvironment
            ).writeFile(toolkit.getFiler(), element.getSourceElement());

            logger.debug("Generating: " + String.format("%sContentValuesBuilder", entity.getQualifiedName()));
            new ClassFileTemplateWriter(
                    String.format("%sContentValuesBuilder", entity.getQualifiedName()),
                    "util/TplEntityContentValuesBuilder.java.tpl",
                    templateEnvironment
            ).writeFile(toolkit.getFiler(), element.getSourceElement());

            logger.debug("Generating: " + String.format("%sRepository", entity.getQualifiedName()));
            new ClassFileTemplateWriter(
                    String.format("%sRepository", entity.getQualifiedName()),
                    "repository/TplEntityRepository.java.tpl",
                    templateEnvironment
            ).writeFile(toolkit.getFiler(), element.getSourceElement());

            EntityRegistry.Singleton.getInstance().addEntity(entity);
        } catch (final IOException e) {
            throw new ProcessorException(e);
        }

        super.onClassElementFound(element, toolkit);
    }

    @Override
    protected void onElementsFound(final List<IElementWrapper> elements, final ProcessorToolkit toolkit) {


        super.onElementsFound(elements, toolkit);
    }
}
