//><#if entity.entityPackageName??>package ${entity.entityPackageName};</#if>

import android.content.ContentValues;

import java.util.Date;

import io.tensai.android.databaseutilities.interfaces.util.IContentValuesBuilder;

/**
 * Created by Aston Hamilton
 */
//>public class ${entity.name}ContentValuesBuilder implements IContentValuesBuilder {
//>    private final ContentValues mContentValues = new ContentValues(${entity.fields?size});
//begin>
    <#list entity.fields as field>
    public ${field.getterJavaType} ${field.getterName}() {
        return ${field.contentValuesGetterWrapperPrefix}mContentValues.${field.conventValuesGetterName}(${entity.name}Contract.COLUMN_${field.name?upper_case})${field.contentValuesGetterWrapperSuffix};
    }
    public ${entity.name}ContentValuesBuilder ${field.setterName}(final ${field.setterJavaType} value) {
        mContentValues.put(${entity.name}Contract.COLUMN_${field.name?upper_case}, ${str_util.format(field.setterValue, "value")});
        return this;
    }
    </#list>
//end>
    @Override
    public ContentValues getValues() {
        return mContentValues;
    }

    @Override
    public void clearValues() {
        mContentValues.clear();
    }
}