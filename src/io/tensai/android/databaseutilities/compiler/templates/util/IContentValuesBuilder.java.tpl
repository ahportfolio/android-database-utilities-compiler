package io.tensai.android.databaseutilities.interfaces.util;

import android.content.ContentValues;

/**
 * Created by Aston Hamilton
 */
public interface IContentValuesBuilder {
    ContentValues getValues();
    void clearValues();
}
