package io.tensai.android.databaseutilitiescompiler.processors.db.openhelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import io.tensai.myapplication.db.contracts.DummyContract;

/**
 * Created by Aston Hamilton
 */
public class AppDatabaseOpenHelper extends SQLiteOpenHelper {
    // Database Info
    public static final String DATABASE_NAME = "appDatabase";
    public static final int DATABASE_VERSION = 1;

    public AppDatabaseOpenHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {

        String CREATE_DUMMY_TABLE = "CREATE TABLE " + DummyContract.TABLE_NAME +
                "(" +
                DummyContract.COLUMN_PK + " INTEGER PRIMARY KEY AUTOINCREMENT," + // Define a primary key
                DummyContract.COLUMN_FIRST_NAME + " TEXT," + // Define a foreign key
                DummyContract.COLUMN_LAST_NAME + " TEXT," +
                DummyContract.COLUMN_AGE + " INTEGER," +
                DummyContract.COLUMN_DATE + " INTEGER" +
                ")";


        db.execSQL(CREATE_DUMMY_TABLE);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {

    }
}
