package io.tensai.android.databaseutilitiescompiler.processors.db.contracts;

import android.provider.BaseColumns;

import io.tensai.myapplication.dbal.query.SQLSelectionStringUtils;

/**
 * Created by Aston Hamilton
 */
public abstract class DummyContract {
    // Table Names
    public static final String TABLE_NAME = "dummy";

    // Dummy Table Columns
    public static final String COLUMN_PK = BaseColumns._ID;
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_DATE = "date";

    // Dummy Table ColumnExpressionBuilder
    public static final SQLSelectionStringUtils.StringFragmentBuilder.ColumnExpressionBuilder BUILDER_PK = SQLSelectionStringUtils.StringFragmentBuilder.forColumn(COLUMN_PK);
    public static final SQLSelectionStringUtils.StringFragmentBuilder.ColumnExpressionBuilder BUILDER_FIRST_NAME = SQLSelectionStringUtils.StringFragmentBuilder.forColumn(COLUMN_FIRST_NAME);
    public static final SQLSelectionStringUtils.StringFragmentBuilder.ColumnExpressionBuilder BUILDER_LAST_NAME = SQLSelectionStringUtils.StringFragmentBuilder.forColumn(COLUMN_LAST_NAME);
    public static final SQLSelectionStringUtils.StringFragmentBuilder.ColumnExpressionBuilder BUILDER_AGE = SQLSelectionStringUtils.StringFragmentBuilder.forColumn(COLUMN_AGE);
    public static final SQLSelectionStringUtils.StringFragmentBuilder.ColumnExpressionBuilder BUILDER_DATE = SQLSelectionStringUtils.StringFragmentBuilder.forColumn(COLUMN_DATE);

}
