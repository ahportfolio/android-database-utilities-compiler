package io.tensai.android.databaseutilitiescompiler.processors.db.repositories;

import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;

import io.tensai.myapplication.db.contracts.DummyContract;
import io.tensai.myapplication.db.data.Dummy;
import io.tensai.myapplication.dbal.repository.AbstractEntityRepository;

/**
 * Created by Aston Hamilton
 */
public class DummyRepository extends AbstractEntityRepository<Dummy> {
    public DummyRepository(final SQLiteOpenHelper openHelper) {
        super(openHelper, DummyContract.TABLE_NAME, DummyContract.COLUMN_PK, new String[]{
                DummyContract.COLUMN_PK,
                DummyContract.COLUMN_FIRST_NAME,
                DummyContract.COLUMN_LAST_NAME,
                DummyContract.COLUMN_AGE,
                DummyContract.COLUMN_DATE
        });
    }

    @Override
    public Dummy buildEntityFromRepositoryCursor(final Cursor cursor) {
        return new Dummy(
                cursor.getLong(0),
                cursor.getString(1),
                new Date(cursor.getLong(4)),
                cursor.getString(2),
                cursor.getInt(3)
        );
    }
}
