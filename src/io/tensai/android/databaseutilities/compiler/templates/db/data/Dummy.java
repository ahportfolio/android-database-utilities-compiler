package io.tensai.android.databaseutilitiescompiler.processors.db.data;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by Aston Hamilton
 */
public class Dummy implements Comparable<Dummy> {
    private long mPk;
    private String mFirstName;
    private String mLastName;
    private int mAge;
    private Date mDate;


    public Dummy(final long pk, final String firstName, final Date date, final String lastName, final int age) {
        mPk = pk;
        mFirstName = firstName;
        mDate = date;
        mLastName = lastName;
        mAge = age;
    }

    public long getPk() {
        return mPk;
    }

    public void setPk(final long pk) {
        mPk = pk;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(final String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(final String lastName) {
        mLastName = lastName;
    }

    public int getAge() {
        return mAge;
    }

    public void setAge(final int age) {
        mAge = age;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(final Date date) {
        mDate = date;
    }

    @Override
    public boolean equals(final Object o) {
        if (o instanceof Dummy) {
            return mPk == ((Dummy) o).getPk()
                    && (mFirstName == null ? ((Dummy) o).getFirstName() == null : mFirstName.equals(((Dummy) o).getFirstName()))
                    && (mLastName == null ? ((Dummy) o).getLastName() == null : mLastName.equals(((Dummy) o).getLastName()))
                    && mAge == ((Dummy) o).getAge()
                    && (mDate == null ? ((Dummy) o).getDate() == null : mDate.equals(((Dummy) o).getDate()));

        }

        return super.equals(o);
    }

    @Override
    public int compareTo(@NonNull final Dummy another) {
        return Dummy.compareInt(
                mLastName.compareTo(another.getLastName()),
                Dummy.compareInt(mFirstName.compareTo(another.getFirstName()),
                        Dummy.compareInt(Dummy.compareInt(mAge, another.getAge()),
                                Dummy.compareInt(mDate.compareTo(another.getDate()),
                                        Dummy.compareLong(mPk, another.getPk()))))
        );
    }


    public static int compareInt(int lhs, int rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }

    public static int compareLong(long lhs, long rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }
}
