//><#if entity.entityPackageName??>package ${entity.entityPackageName};</#if>


import io.tensai.android.databaseutilities.dbal.query.SQLSelectionStringUtils;

/**
 * Created by Aston Hamilton
 */
//>public abstract class ${entity.name}Contract {
    // Table Names
//>    public static final String TABLE_NAME = "${entity.tableName}";

//>    // ${entity.name} Table Columns
//begin>
<#list entity.fields as field>
    public static final String COLUMN_${field.name?upper_case} = "${field.name?lower_case}";
</#list>
//end>
//>    // ${entity.name} Table ColumnExpressionBuilder
//begin>
<#list entity.fields as field>
    public static final SQLSelectionStringUtils.StringFragmentBuilder.ColumnExpressionBuilder BUILDER_${field.name?upper_case} = SQLSelectionStringUtils.StringFragmentBuilder.forColumn(COLUMN_${field.name?upper_case});
</#list>
//end>
}
