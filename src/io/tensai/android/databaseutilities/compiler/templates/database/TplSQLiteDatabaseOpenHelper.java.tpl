//><#if database.openHelperPackageName?length != 0>package ${database.openHelperPackageName};</#if>

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//begin>
<#list database.entities as entity>
<#if entity.entityPackageName??>import ${entity.entityPackageName}.${entity.name}Contract;</#if>
</#list>
//end>

/**
 * Created by Aston Hamilton
 */
//>public class ${database.openHelperSimpleName} extends SQLiteOpenHelper {
    // Database Info
//>    public static final String DATABASE_NAME = "${database.fileName}";
//>    public static final int DATABASE_VERSION = ${database.version};


//>    public ${database.openHelperSimpleName}(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        String SQL;

        db.beginTransaction();
        try {
//begin>
            <#list database.entities as entity>
            SQL = "CREATE TABLE " + ${entity.name}Contract.TABLE_NAME +
                    "(" +
                    <#list entity.fields as field>
                    ${entity.name}Contract.COLUMN_${field.name?upper_case} + " ${field.SQLType}<#if field == entity.primaryKey> PRIMARY KEY</#if><#if field == entity.primaryKey || field.autoIncrement> AUTOINCREMENT</#if><#if !field?is_last>,</#if>" +
                    </#list>
                    ")";

            db.execSQL(SQL);
            <#list entity.indices as entityIndex>
            SQL = "CREATE<#if entityIndex.unique> UNIQUE</#if> INDEX ${entityIndex.name} ON " + ${entity.name}Contract.TABLE_NAME +
                    "(" +
                    <#list entityIndex.fields as field>
                    ${entity.name}Contract.COLUMN_${field.name?upper_case}<#if !field?is_last> + ","</#if> +
                    </#list>
                    ")<#if entityIndex.criteria?length != 0> WHERE ${entityIndex.criteria}</#if>";

            db.execSQL(SQL);
            </#list>
            </#list>
//end>
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        String SQL;

        db.beginTransaction();
        try {
//begin>
            <#list database.entities as entity>
            SQL = "DROP TABLE IF EXISTS " + ${entity.name}Contract.TABLE_NAME;
            db.execSQL(SQL);
            </#list>
//end>
            onCreate(db);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onDowngrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
