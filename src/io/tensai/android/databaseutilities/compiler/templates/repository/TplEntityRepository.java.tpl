//><#if entity.entityPackageName??>package ${entity.entityPackageName};</#if>

import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;

import io.tensai.android.databaseutilities.dbal.repository.AbstractEntityRepository;
/**
 * Created by Aston Hamilton
 */
//>public class ${entity.name}Repository extends AbstractEntityRepository<${entity.name}> {
//>     public ${entity.name}Repository(final SQLiteOpenHelper openHelper) {
//>         super(openHelper, ${entity.name}Contract.TABLE_NAME, ${entity.name}Contract.COLUMN_${entity.primaryKey.name?upper_case}, new String[]{
//begin>
                <#list entity.fields as field>
                 ${entity.name}Contract.COLUMN_${field.name?upper_case}<#if !field?is_last>,</#if>
                </#list>
//end>
         });
     }

     @Override
//>     public ${entity.name} buildEntityFromRepositoryCursor(final Cursor cursor) {
//>         return new ${entity.name}(
//begin>
                <#list entity.constructor.parameters as param>
                 ${str_util.format(param.cursorAccessor, "cursor")}<#if !param?is_last>,</#if>
                </#list>
//end>
         );
     }
}
