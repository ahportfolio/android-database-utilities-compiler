package io.tensai.android.databaseutilities.dbal.repository;

import android.content.Context;

import io.tensai.android.databaseutilities.interfaces.repository.IEntityRepository;
import io.tensai.android.databaseutilities.interfaces.repository.IEntityRepositoryFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aston
 */
public class DefaultEntityRepositoryFactory implements IEntityRepositoryFactory {
    private final Map<Class, RepositoryBuilder> mRegisteredEntities = new HashMap<>();

    public DefaultEntityRepositoryFactory() {

//begin>
<#list databases as database>
<#list database.entities as entity>
        mRegisteredEntities.put(<#if entity.entityPackageName??>${entity.entityPackageName}.</#if>${entity.name}.class, new ${entity.name}RepositoryBuilder${database?index}${entity?index}());
</#list>
</#list>
//end>

    }

    @Override
    public <T> boolean isEntitySupported(final Class<T> entityClazz) {
        return mRegisteredEntities.containsKey(entityClazz);
    }

    @Override
    public <T> IEntityRepository<T> buildEntityRepository(final Context context, final Class<T> entityClazz) {
        return mRegisteredEntities.get(entityClazz).build(context);
    }

    private interface RepositoryBuilder {
        <T> IEntityRepository<T> build(final Context context);
    }

//begin>
<#list databases as database>
<#list database.entities as entity>
    @SuppressWarnings("unchecked")
    private static class ${entity.name}RepositoryBuilder${database?index}${entity?index} implements RepositoryBuilder {
        @Override
        public IEntityRepository<<#if entity.entityPackageName??>${entity.entityPackageName}.</#if>${entity.name}> build(final Context context) {
            return new <#if entity.entityPackageName??>${entity.entityPackageName}.</#if>${entity.name}Repository(
                new ${database.openHelperQualifiedName}(context)
            );
        }
    }
</#list>
</#list>
//end>
}
