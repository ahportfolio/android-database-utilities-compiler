package io.tensai.android.databaseutilities.interfaces.repository;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

/**
 * Created by Aston Hamilton
 */
public interface IEntityRepository<T> {
    Cursor findByPkWithCursorResult(final long pk);

    Cursor findAllWithCursorResult();

    Cursor findAllWithCursorResult(final String orderBy);

    Cursor findAllWithCursorResult(final String orderBy, final int offset, final int limit);

    Cursor findWithCursorResult(final String selection, String[] selectionArgs);

    Cursor findWithCursorResult(final String selection, String[] selectionArgs, final String orderBy);

    Cursor findWithCursorResult(final String selection, String[] selectionArgs, final String orderBy, final int offset, final int limit);

    long insert(final ContentValues values);

    long insertOrThrow(final ContentValues values);

    long insertWithOnConflict(final ContentValues values, final int conflictAlgorithm);

    int updateAll(final ContentValues values);

    int update(final ContentValues values, final String whereClause, String[] whereArgs);

    int updateWithOnConflict(final ContentValues values, final String whereClause, String[] whereArgs, final int conflictAlgorithm);

    int deleteAll();

    int delete(final String whereClause, String[] whereArgs);

    long count();

    long count(final String selection, String[] selectionArgs);

    T findByPk(final long pk);

    List<T> findAll();

    List<T> findAll(final String orderBy);

    List<T> findAll(final String orderBy, final int offset, final int limit);

    List<T> find(final String selection, String[] selectionArgs);

    List<T> find(final String selection, String[] selectionArgs, final String orderBy);

    List<T> find(final String selection, String[] selectionArgs, final String orderBy, final int offset, final int limit);

    SQLiteOpenHelper getOpenHelper();

    T buildEntityFromRepositoryCursor(final Cursor cursor);

    void close();
}
