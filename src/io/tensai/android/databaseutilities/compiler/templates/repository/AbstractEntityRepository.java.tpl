package io.tensai.android.databaseutilities.dbal.repository;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import io.tensai.android.databaseutilities.interfaces.repository.IEntityRepository;

/**
 * Created by Aston Hamilton
 */
public abstract class AbstractEntityRepository<T> implements IEntityRepository<T> {
    private final SQLiteOpenHelper mOpenHelper;
    private final String mTableName;
    private final String mEntityPkColumn;
    private final String[] mProjectionEntityColumns;

    public AbstractEntityRepository(final SQLiteOpenHelper openHelper, final String tableName, final String entityPkColumn, final String[] projectionEntityColumns) {
        mOpenHelper = openHelper;
        mTableName = tableName;
        mEntityPkColumn = entityPkColumn;
        mProjectionEntityColumns = projectionEntityColumns;
    }

    @Override
    public Cursor findByPkWithCursorResult(final long pk) {
        return mOpenHelper.getReadableDatabase().query(
                mTableName,
                mProjectionEntityColumns,
                mEntityPkColumn + " = ?",
                new String[]{String.valueOf(pk)},
                null,
                null,
                null,
                "1"
        );
    }

    @Override
    public Cursor findAllWithCursorResult() {
        return findAllWithCursorResult(mEntityPkColumn + " ASC");
    }

    @Override
    public Cursor findAllWithCursorResult(final String orderBy) {
        return mOpenHelper.getReadableDatabase().query(
                mTableName,
                mProjectionEntityColumns,
                null,
                null,
                null,
                null,
                orderBy
        );
    }

    @Override
    public Cursor findAllWithCursorResult(final String orderBy, final int offset, final int limit) {
        return mOpenHelper.getReadableDatabase().query(
                mTableName,
                mProjectionEntityColumns,
                null,
                null,
                null,
                null,
                orderBy,
                offset + ", " + limit
        );
    }

    @Override
    public Cursor findWithCursorResult(final String selection, final String[] selectionArgs) {
        return findWithCursorResult(selection, selectionArgs, mEntityPkColumn + " ASC");
    }

    @Override
    public Cursor findWithCursorResult(final String selection, final String[] selectionArgs, final String orderBy) {
        return mOpenHelper.getReadableDatabase().query(
                mTableName,
                mProjectionEntityColumns,
                selection,
                selectionArgs,
                null,
                null,
                orderBy,
                null
        );
    }

    @Override
    public Cursor findWithCursorResult(final String selection, final String[] selectionArgs, final String orderBy, final int offset, final int limit) {
        return mOpenHelper.getReadableDatabase().query(
                mTableName,
                mProjectionEntityColumns,
                selection,
                selectionArgs,
                null,
                null,
                orderBy,
                offset + ", " + limit
        );
    }

    @Override
    public long insert(final ContentValues values) {
        return mOpenHelper.getWritableDatabase().insert(
                mTableName,
                null,
                values
        );
    }

    @Override
    public long insertOrThrow(final ContentValues values) {
        return mOpenHelper.getWritableDatabase().insertOrThrow(
                mTableName,
                null,
                values
        );
    }

    @Override
    public long insertWithOnConflict(final ContentValues values, final int conflictAlgorithm) {
        return mOpenHelper.getWritableDatabase().insertWithOnConflict(
                mTableName,
                null,
                values,
                conflictAlgorithm
        );
    }

    @Override
    public int updateAll(final ContentValues values) {
        return mOpenHelper.getWritableDatabase().update(
                mTableName,
                values,
                null,
                null
        );
    }

    @Override
    public int update(final ContentValues values, final String whereClause, final String[] whereArgs) {
        return mOpenHelper.getWritableDatabase().update(
                mTableName,
                values,
                whereClause,
                whereArgs
        );
    }

    @Override
    public int updateWithOnConflict(final ContentValues values, final String whereClause, final String[] whereArgs, final int conflictAlgorithm) {
        return mOpenHelper.getWritableDatabase().updateWithOnConflict(
                mTableName,
                values,
                whereClause,
                whereArgs,
                conflictAlgorithm
        );
    }

    @Override
    public int deleteAll() {
        return mOpenHelper.getWritableDatabase().delete(
                mTableName,
                null, null
        );
    }

    @Override
    public int delete(final String whereClause, final String[] whereArgs) {
        return mOpenHelper.getWritableDatabase().delete(
                mTableName,
                whereClause, whereArgs
        );
    }

    @Override
    public long count() {
        final long count;
        final Cursor cursor = mOpenHelper.getReadableDatabase().rawQuery(
                "SELECT COUNT(*) FROM " + mTableName,
                null
        );
        cursor.moveToFirst();
        count = cursor.getLong(0);
        cursor.close();
        return count;
    }

    @Override
    public long count(final String selection, final String[] selectionArgs) {
        final long count;
        final Cursor cursor = mOpenHelper.getReadableDatabase().rawQuery(
                "SELECT COUNT(*) FROM " + mTableName + (selection == null ? "" : " WHERE " + selection),
                selectionArgs
        );

        cursor.moveToFirst();
        count = cursor.getLong(0);
        cursor.close();
        return count;
    }

    @Override
    public T findByPk(final long pk) {
        final Cursor cursor = findByPkWithCursorResult(pk);
        final T entity;

        if (!cursor.moveToFirst()) {
            return null;
        }

        entity = buildEntityFromRepositoryCursor(cursor);
        cursor.close();

        return entity;
    }

    @Override
    public List<T> findAll() {
        final Cursor cursor = findAllWithCursorResult();
        final List<T> entities = new ArrayList<>(cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                entities.add(buildEntityFromRepositoryCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return entities;
    }

    @Override
    public List<T> findAll(final String orderBy) {
        final Cursor cursor = findAllWithCursorResult(orderBy);
        final List<T> entities = new ArrayList<>(cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                entities.add(buildEntityFromRepositoryCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return entities;
    }

    @Override
    public List<T> findAll(final String orderBy, final int offset, final int limit) {
        final Cursor cursor = findAllWithCursorResult(orderBy, offset, limit);
        final List<T> entities = new ArrayList<>(cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                entities.add(buildEntityFromRepositoryCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return entities;
    }

    @Override
    public List<T> find(final String selection, final String[] selectionArgs) {
        final Cursor cursor = findWithCursorResult(selection, selectionArgs);
        final List<T> entities = new ArrayList<>(cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                entities.add(buildEntityFromRepositoryCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return entities;
    }

    @Override
    public List<T> find(final String selection, final String[] selectionArgs, final String orderBy) {
        final Cursor cursor = findWithCursorResult(selection, selectionArgs, orderBy);
        final List<T> entities = new ArrayList<>(cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                entities.add(buildEntityFromRepositoryCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return entities;
    }

    @Override
    public List<T> find(final String selection, final String[] selectionArgs, final String orderBy, final int offset, final int limit) {
        final Cursor cursor = findWithCursorResult(selection, selectionArgs, orderBy, offset, limit);
        final List<T> entities = new ArrayList<>(cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                entities.add(buildEntityFromRepositoryCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return entities;
    }

    @Override
    public SQLiteOpenHelper getOpenHelper() {
        return mOpenHelper;
    }

    public void close() {
        mOpenHelper.close();
    }
}
